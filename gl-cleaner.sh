#! /bin/env bash
gitlab-ctl stop
gitlab-ctl cleanse
gitlab-ctl uninstall
rm -rf /opt/gitlab/ /var/opt/gitlab /etc/gitlab /var/log/gitlab/
apt remove gitlab-ee
